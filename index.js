const TelegramBot = require('node-telegram-bot-api');
const axios = require('axios');

const token = '5827096041:AAFzgtW4GVHl7WmQzUHn4yQORv9rOdxWGFI';
const bot = new TelegramBot(token, { polling: true });

let botUsername;
bot.getMe().then((info) => {
  botUsername = info.username;
});

const EMOJIS = {
  basketball: '🏀',
  soccer: '⚽️',
  dice: '🎲',
  dart: '🎯',
  bowling: '🎳',
};

const DELAY_TIME = 700;

const keyboard = {
  inline_keyboard: [
    [
      { text: 'Classic', callback_data: 'classic' },
      { text: 'Advanced', callback_data: 'advanced' },
    ],
  ],
};

bot.onText(/\/start/, async (msg) => {
  try {
    await bot.sendMessage(
      msg.chat.id,
      'You can use that bot privately or add it to any group/channel.\nJust use commands: /classic or /advanced to roll different dices.',
    );
  } catch (err) {
    console.error('error onStart');
  }
});

bot.onText(/^\/classic/, async (message) => {
  onClassic(message);
});

bot.onText(/^\/advanced/, (message) => {
  onAdvanced(message);
});

const emojiMatch = {
  f: '⚽️',
  p: '🏀',
  b: '🎲',
  d: '🎯',
  w: '🎳',
};

bot.on('inline_query', async (query) => {
  let classicTotal = 0;
  const classicResults = 'fff ppp'
      .split('')
      .map((w) => {
        if (w === ' ') {
          return ' ';
        }

        if (Math.random() < 0.6) {
          classicTotal++;
          return emojiMatch[w];
        } else {
          return '🚫';
        }
      })
      .join('');

  let advancedTotal = 0;
  const advancedResults = 'bdw'
      .split('')
      .map((w) => `${emojiMatch[w]}: ${new Array(6).fill(0).reduce((acc, val, index) => {
        const prevResult = acc[index - 1];
        
        if (w === 'b' && index === 0) {
          advancedTotal++;
          return '1';
        }
        
        if (prevResult !== '0') {
          const result = Math.random() < 0.8 ? '1' : '0';
          if (result !== '0') {
            advancedTotal++;
          }
          
          return `${acc}${result}`;
        }
        
        return `${acc}0`;
      }, '')
          .split('')
          .map((v) => v === '1' ? '⚡️' : '🚫')
          .join('')}`)
      .join('\n');

  try {
    await axios
      .get(
        'https://api.telegram.org/bot' + token + '/answerInlineQuery',
        {
          params: {
            inline_query_id: query.id,
            results: JSON.stringify([
              {
                type: 'article',
                id: '1',
                title: 'Classic',
                thumbnail_url: 'https://res.cloudinary.com/alain-corp-cdn/image/upload/v1682785502/p9bkan8snetqwtkzfppu.png',
                input_message_content: {
                  message_text: `${classicResults}\n\nTotal: ${classicTotal}`,
                },
              },
              {
                type: 'article',
                id: '2',
                title: 'Advanced',
                thumbnail_url: 'https://res.cloudinary.com/alain-corp-cdn/image/upload/v1682790094/z46dkoziw5m9dqaoykho.png',
                input_message_content: {
                  message_text: `${advancedResults}\n\nTotal: ${advancedTotal}`,
                }
              },
              {
                type: 'article',
                id: '3',
                title: 'More',
                thumbnail_url: 'https://res.cloudinary.com/alain-corp-cdn/image/upload/v1682838424/oo4ybwbi9cwfh8vt9vck.png',
                input_message_content: {
                  message_text: `More FUNctions 👉🏻 @${botUsername}`,
                }
              }
            ]),
            cache_time: 1,
            is_personal: true,
          },
        },
        {
          headers: {
            'Content-Type': 'application/json',
          },
        },
      )
      .then((res) => {
        console.log('res', res.status);
      })
      .catch((err) => {
        console.error('err', err.message);
      });
  } catch (err) {
    console.error('axios error', err.message);
  }
});

bot.on('callback_query', async (query) => {
  const message = {
    ...query.message,
    from: query.from,
  };
  if (query.data === 'classic') {
    await onClassic(message);
  } else {
    await onAdvanced(message);
  }
});

const onClassic = async (message) => {
  const chatId = message.chat.id;

  try {
    const initialMessage = await bot.sendMessage(
      chatId,
      `Rolling dice for @${message.from?.username ?? 'anonymous'}...`,
      {
        reply_to_message_id: message.message_id,
      },
    );

    let diceValues = [];
    let winCountSum = 0;
    let countText = '';

    const diceTypes = [EMOJIS.basketball, EMOJIS.soccer];
    for (let i = 0; i < 4; i++) {
      const roll = await bot.sendDice(chatId, {
        emoji: diceTypes[Math.floor(i / 2)],
        reply_to_message_id: initialMessage.message_id,
      });
      const value = roll.dice.value;
      diceValues.push(value);

      if ((i < 2 && value >= 4) || (i >= 2 && value >= 3)) {
        winCountSum++;
      }

      await delay(DELAY_TIME);
    }

    await bot.sendMessage(chatId, `${countText}@${message.from?.username}'s win count: ${winCountSum}`, {
      reply_markup: keyboard,
      reply_to_message_id: initialMessage.message_id,
    });
  } catch (err) {
    await bot
      .sendMessage(message.chat.id, `Error onClassic: ${err.message}`, {
        reply_to_message_id: message.message_id,
      })
      .catch((err) => {
        console.error("Error onClassic: can't send error message", err.message);
      });
  }
};

const onAdvanced = async (message) => {
  try {
    const chatId = message.chat.id;
    const initialMessage = await bot.sendMessage(
      chatId,
      `Rolling dice for @${message.from?.username ?? 'anonymous'}...`,
      {
        reply_to_message_id: message.message_id,
      },
    );

    let diceValues = [];
    let winCountSum = 0;
    let countText = '';

    const diceTypes = [EMOJIS.dice, EMOJIS.dart, EMOJIS.bowling];
    const diceCounts = [0, 0, 0];

    for (let i = 0; i < 3; i++) {
      const roll = await bot.sendDice(chatId, {
        emoji: diceTypes[i],
        reply_to_message_id: initialMessage.message_id,
      });
      const value = roll.dice.value;
      diceValues.push(value);
      diceCounts[i] = value;
      await delay(DELAY_TIME);
    }

    winCountSum = diceCounts.reduce((a, b) => a + b, 0);
    countText = `Dice values: ${diceCounts[0]} on ${EMOJIS.dice}, ${diceCounts[1]} on ${EMOJIS.dart}, and ${diceCounts[2]} on ${EMOJIS.bowling}. `;

    await bot.sendMessage(chatId, `${countText}@${message.from?.username}'s win count: ${winCountSum}`, {
      reply_markup: keyboard,
      reply_to_message_id: initialMessage.message_id,
    });
  } catch (err) {
    await bot
      .sendMessage(message.chat.id, `Error onAdvanced: ${err.message}`, {
        reply_to_message_id: message.message_id,
      })
      .catch((err) => {
        console.error("Error onAdvanced: can't send error message", err.message);
      });
  }
};

function delay(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

console.info('Script started successfully');
