FROM node:16

ENV NODE_ENV production

WORKDIR /app

EXPOSE 9229

CMD bash -c "npm run start"
